import Badge, { BadgeColor } from 'components/badge';
import { GiSwordInStone } from 'react-icons/gi';
import {
    achievements,
    daegon,
    elwan,
    fredrick,
    lurtz,
    tyron
} from 'conf/badgeData';
import { ParchmentLayer } from 'components/parchment/Parchment';

function shuffle(array: any[]) {
    return array.sort(() => Math.random() - 0.5);
}

export default function BadgePage() {
    console.log('daegon', shuffle(daegon));
    console.log('elwan', shuffle(elwan));
    console.log('fredrick', shuffle(fredrick));
    console.log('lurtz', shuffle(lurtz));
    console.log('tyron', shuffle(tyron));

    return (
        <div className='relative overflow-y-auto h-screen z-0 '>
            <div className='flex flex-wrap justify-evenly w-full'>
                {achievements.map(item => (
                    <Badge
                        id={item.id}
                        key={item.label}
                        badgeColor={item.color}
                        label={item.label}
                        icon={item.icon}
                    />
                ))}

                <Badge
                    id={400}
                    badgeColor={BadgeColor.silver}
                    label='Arma Leggendaria'
                    icon={GiSwordInStone}
                />
            </div>
        </div>
    );
}
