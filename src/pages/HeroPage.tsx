import Badge, { BadgeColor } from 'components/badge';
import { ParchmentLayer } from 'components/parchment/Parchment';
import {
    achievements,
    daegon,
    elwan,
    fredrick,
    lurtz,
    tyron
} from 'conf/badgeData';
import { Fragment, useMemo, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { TiArrowBackOutline } from 'react-icons/ti';
import { Dialog, Transition } from '@headlessui/react';
import { IconType } from 'react-icons';

export default function HeroPage() {
    const { id } = useParams();
    const navigate = useNavigate();

    if (!id) {
        navigate(-1);
    }

    const [currentBadge, setCurrentBadge] = useState<{
        id: number;
        label: string;
        icon: IconType;
        description: string;
        color: BadgeColor;
    }>();

    const badges = useMemo(() => {
        switch (id) {
            case 'daegon':
                return achievements.filter(v => daegon.includes(v.id));
            case 'elwan':
                return achievements.filter(v => elwan.includes(v.id));
            case 'fredrick':
                return achievements.filter(v => fredrick.includes(v.id));
            case 'lurtz':
                return achievements.filter(v => lurtz.includes(v.id));
            case 'tyron':
                return achievements.filter(v => tyron.includes(v.id));
            default:
                return [];
        }
    }, [id]);

    return (
        <>
            <Transition appear show={!!currentBadge} as={Fragment}>
                <Dialog
                    as='div'
                    className='relative z-10'
                    onClose={() => {
                        setCurrentBadge(undefined);
                    }}>
                    <Transition.Child
                        as={Fragment}
                        enter='ease-out duration-300'
                        enterFrom='opacity-0'
                        enterTo='opacity-100'
                        leave='ease-in duration-200'
                        leaveFrom='opacity-100'
                        leaveTo='opacity-0'>
                        <div className='fixed inset-0 bg-black bg-opacity-40' />
                    </Transition.Child>

                    <div className='fixed inset-0 overflow-y-auto'>
                        <div className='flex min-h-full items-center justify-center text-center'>
                            <Transition.Child
                                as={Fragment}
                                enter='ease-out duration-300'
                                enterFrom='opacity-0 scale-95'
                                enterTo='opacity-100 scale-100'
                                leave='ease-in duration-200'
                                leaveFrom='opacity-100 scale-100'
                                leaveTo='opacity-0 scale-95'>
                                <Dialog.Panel
                                    className={` relative mx-4 w-full max-w-md overflow-hidden rounded-2xl px-6 py-4 text-left align-middle shadow-xl ${currentBadge?.color} `}>
                                    <Dialog.Title
                                        as='h3'
                                        className='text-xl font-FellGreat font-semibold text-gray-900'>
                                        {currentBadge?.label}
                                    </Dialog.Title>
                                    <div className='mt-2 darkGrey px-4 py-2 rounded-xl min-h-[60px]'>
                                        <p className='text font-FellGreat text-white'>
                                            {currentBadge?.description}
                                        </p>
                                    </div>
                                </Dialog.Panel>
                            </Transition.Child>
                        </div>
                    </div>
                </Dialog>
            </Transition>
            <div className='overflow-y-auto h-full relative z-0 '>
                <div className='sticky z-10 top-2 w-full '>
                    <div className='relative h-16  max-w-xs px-2 ml-4'>
                        <ParchmentLayer />
                        <div className='relative flex flex-col md:w-64 h-full '>
                            <h1 className='text-3xl md:text-5xl tracking-wider font-semibold font-FellGreat w-full text-center h-full capitalize mt-8 transition-transform -translate-y-1/2'>
                                {id}
                            </h1>
                        </div>
                        <TiArrowBackOutline
                            className='absolute right-4 h-8 w-8 top-1/2 transition-transform -translate-y-1/2 rounded-full cursor-pointer '
                            onClick={() => {
                                navigate(-1);
                            }}
                        />
                    </div>
                </div>
                <div className='inline-flex flex-wrap justify-around w-full z-0 '>
                    {badges.map(item => (
                        <Badge
                            id={item.id}
                            key={item.label}
                            badgeColor={item.color}
                            label={item.label}
                            icon={item.icon}
                            onClick={() => {
                                setCurrentBadge(item);
                            }}
                        />
                    ))}
                </div>
            </div>
        </>
    );
}
