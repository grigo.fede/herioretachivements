import { Dialog, Transition } from '@headlessui/react';
import Parchment from 'components/parchment/Parchment';
import { Fragment, useMemo, useState } from 'react';

import { HiRefresh } from 'react-icons/hi';

export default function HomePage() {
    const [open, setOpen] = useState(false);

    const showFarewell = useMemo(() => {
        return !!localStorage.getItem('Fredrick') &&
            !!localStorage.getItem('Tyron') &&
            !!localStorage.getItem('Lurtz') &&
            !!localStorage.getItem('Elwan') &&
            !!localStorage.getItem('Daegon')
            ? true
            : false;
    }, []);

    return (
        <>
            <div className='flex flex-col h-full w-full  '>
                <div className=' relative h-16  sm:h-20 darkGrey border-b border-b-gray-700 flex items-center px-4'>
                    <h1 className='text-3xl sm:text-5xl font-FellGreat text-white'>
                        Eroi di Herioret
                    </h1>
                    <HiRefresh
                        className='text-white absolute sm:h-10 sm:w-10 w-7 h-7  right-4 sm:right-12  hover:animate-reverse-spin cursor-pointer'
                        onClick={() => {
                            localStorage.clear();
                        }}
                    />
                </div>
                <div className=' md:snap-x overflow-auto scrollbar scrollbar-thin scrollbar-thumb-green-900 scrollbar-track-gray-100 h-full w-full py-8 transition-all duration-500 '>
                    <div className='flex flex-col md:flex-row flex-nowrap px-4 md:px-8 h-full space-y-4 md:space-x-8 w-full items-start '>
                        <span className='block h-full min-w-min'> . </span>

                        <Parchment
                            title='Daegon'
                            subtitle="L'Inevitabile"
                            quote={
                                'Ogni cosa ha una sua fine, ogni vita è destinata al giudizio. Non potete fuggire, io vi osservo sempre'
                            }
                        />

                        <Parchment
                            title='Elwan'
                            subtitle='Il Nosferatu'
                            quote={`Non rimpiango nulla, libero sono sceso nella notte e libero la esplorerò. L'unico limite, è la mia volontà`}
                        />

                        <Parchment
                            title='Fredrick'
                            subtitle={`Il fondatore`}
                            quote={`Adesso riempite i boccali e cantate la mia Saga! La vita è troppo breve per non festeggiare e certe storie vanno ricordate!`}
                            secret={`L'ultimo dei Mortali`}
                        />

                        <Parchment
                            title='Lurtz'
                            subtitle='La Sentinella della Fine dei Tempi'
                            quote={`Come l'acciaio sono stato forgiato, come la fede sono stato provato, e ora la mia veglia comincia`}
                        />

                        <Parchment
                            title='Tyron'
                            subtitle='Il Custode dei Sogni'
                            quote={
                                'No, non sparirò mica, mi troverai sempre qui la notte, dietro i tuoi occhi chiusi, a proteggere i tuoi Sogni '
                            }
                        />

                        <span className='block h-full min-w-min'> . </span>
                    </div>
                </div>
                {showFarewell && (
                    <div
                        onClick={() => setOpen(true)}
                        className=' h-16 sm:h-18 darkGrey border-b border-b-gray-700 flex items-center px-4 w-full cursor-pointer'>
                        <h1 className='text-xl sm:text-3xl font-FellGreat text-white text-center  animate-pulse w-full select-none cursor-pointer'>
                            L'ultimo paragrafo
                        </h1>
                    </div>
                )}
            </div>
            <Transition appear show={open} as={Fragment}>
                <Dialog
                    as='div'
                    className='relative z-10'
                    onClose={() => {
                        setOpen(false);
                    }}>
                    <Transition.Child
                        as={Fragment}
                        enter='ease-out duration-300'
                        enterFrom='opacity-0'
                        enterTo='opacity-100'
                        leave='ease-in duration-200'
                        leaveFrom='opacity-100'
                        leaveTo='opacity-0'>
                        <div className='fixed inset-0 bg-black bg-opacity-40' />
                    </Transition.Child>

                    <div className='fixed inset-0 overflow-y-auto'>
                        <div className='flex min-h-full items-center justify-center text-center'>
                            <Transition.Child
                                as={Fragment}
                                enter='ease-out duration-300'
                                enterFrom='opacity-0 scale-95'
                                enterTo='opacity-100 scale-100'
                                leave='ease-in duration-200'
                                leaveFrom='opacity-100 scale-100'
                                leaveTo='opacity-0 scale-95'>
                                <Dialog.Panel
                                    className={` relative mx-4 w-full max-w-md overflow-hidden rounded-2xl px-6 py-4 text-left align-middle shadow-xl silver `}>
                                    <Dialog.Title
                                        as='h3'
                                        className='text-xl font-FellGreat font-semibold text-gray-900'>
                                        {'Un ultima parola era doverosa ... '}
                                    </Dialog.Title>
                                    <div className='mt-2 darkGrey px-4 py-2 rounded-xl min-h-[60px] text-lg'>
                                        <p className=' font-FellGreat text-white mb-2'>
                                            {`E’ giunta al termine anche questa avventura.`}
                                        </p>
                                        <p className=' font-FellGreat text-white mb-2'>
                                            {`Non avrei mai immaginato che sarebbe durata tanto, ma soprattutto che avrebbe virato così tanto dall’idea iniziale: avete letteralmente mandato in aria metà dei miei piani, avete trasformato momenti che avrei considerato secondari in vere e proprie saghe epiche, e allo stesso tempo ho dovuto cestinare un sacco di idee e di avventure, semplicemente perchè avete scelto diversamente. Ma ne sono stato veramente felice , perchè così questa storia è stata scritta da tutti quanti noi, assieme, è frutto delle nostre menti folli, così come delle menti di quelli che non sono rimasti fino alla fine. `}
                                        </p>
                                        <p className=' font-FellGreat text-white mb-2'>
                                            {`Sono orgoglioso di quello che abbiamo fatto e grazie, grazie davvero di aver sopportato per sei anni questo sconclusionato Narratore. `}
                                        </p>
                                        <p className=' font-FellGreat text-white mb-2'>
                                            {`Posso dire, che la penna ora può finalmente riposare.`}
                                        </p>
                                        <p className=' font-FellGreat text-white mt-16'>
                                            {`... Almeno per un po' ... `}
                                        </p>
                                    </div>
                                </Dialog.Panel>
                            </Transition.Child>
                        </div>
                    </div>
                </Dialog>
            </Transition>
        </>
    );
}
