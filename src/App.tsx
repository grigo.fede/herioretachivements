import { ParchmentEffectGenerator } from 'components/parchment/Parchment';

import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import BadgePage from 'pages/BadgePage';
import HomePage from 'pages/Home';
import HeroPage from 'pages/HeroPage';

const router = createBrowserRouter([
    {
        path: '/',
        element: <HomePage />
    },
    {
        path: '/test',
        element: <BadgePage />
    },
    { path: '/hero/:id', element: <HeroPage /> }
]);

export default function App(): JSX.Element {
    return (
        <>
            <ParchmentEffectGenerator />
            <main className='h-screen min-h-screen '>
                <div className="inset-0 h-full overflow-hidden relative bg-[url('https://images.unsplash.com/photo-1546484396-fb3fc6f95f98?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80')] bg-cover">
                    <RouterProvider router={router} />
                </div>
            </main>
        </>
    );
}
