import { IconType } from 'react-icons';

export enum BadgeColor {
    yellow = 'yellow',
    orange = 'orange',
    pink = 'pink',
    red = 'red', // combattimento
    purple = 'purple', // Extra
    teal = 'teal', // quest
    blue = 'blue', // png
    blueDark = 'blue-dark', // lore
    green = 'green', // crafting
    greenDark = 'green-dark', // craftomh
    silver = 'silver',
    gold = 'gold' // meccaniche
}

export default function Badge(props: {
    id: number;
    icon?: IconType;
    label: string;
    badgeColor: BadgeColor;
    onClick?: () => void;
}) {
    return (
        <div
            className={`badge ${props.badgeColor} cursor-pointer`}
            onClick={props.onClick}>
            <div className='circle'>
                {props.icon && (
                    <props.icon className='w-8 absolute left-1/2 -translate-x-1/2 top-1/2 -translate-y-1/2 -mt-1.5' />
                )}
                <div className='ribbon left-1/2 -translate-x-1/2 cursor-pointer'>
                    {props.label}
                </div>
            </div>
        </div>
    );
}
