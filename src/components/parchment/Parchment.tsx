import { useMemo, useState } from 'react';
import { Transition } from '@headlessui/react';
import { useWindowSize } from 'usehooks-ts';
import { useNavigate } from 'react-router-dom';
import { GiDiceTwentyFacesTwenty } from 'react-icons/gi';

export function ParchmentLayer() {
    return (
        <div className="absolute flex  w-full inset-0 -left-1  shadow-[2px 3px 20px black, 0 0 125px #8f5922 inset] bg-[#fffef0] [filter:url(#pergamena)] bg-[url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAMAAAAp4XiDAAAAUVBMVEWFhYWDg4N3d3dtbW17e3t1dXWBgYGHh4d5eXlzc3OLi4ubm5uVlZWPj4+NjY19fX2JiYl/f39ra2uRkZGZmZlpaWmXl5dvb29xcXGTk5NnZ2c8TV1mAAAAG3RSTlNAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEAvEOwtAAAFVklEQVR4XpWWB67c2BUFb3g557T/hRo9/WUMZHlgr4Bg8Z4qQgQJlHI4A8SzFVrapvmTF9O7dmYRFZ60YiBhJRCgh1FYhiLAmdvX0CzTOpNE77ME0Zty/nWWzchDtiqrmQDeuv3powQ5ta2eN0FY0InkqDD73lT9c9lEzwUNqgFHs9VQce3TVClFCQrSTfOiYkVJQBmpbq2L6iZavPnAPcoU0dSw0SUTqz/GtrGuXfbyyBniKykOWQWGqwwMA7QiYAxi+IlPdqo+hYHnUt5ZPfnsHJyNiDtnpJyayNBkF6cWoYGAMY92U2hXHF/C1M8uP/ZtYdiuj26UdAdQQSXQErwSOMzt/XWRWAz5GuSBIkwG1H3FabJ2OsUOUhGC6tK4EMtJO0ttC6IBD3kM0ve0tJwMdSfjZo+EEISaeTr9P3wYrGjXqyC1krcKdhMpxEnt5JetoulscpyzhXN5FRpuPHvbeQaKxFAEB6EN+cYN6xD7RYGpXpNndMmZgM5Dcs3YSNFDHUo2LGfZuukSWyUYirJAdYbF3MfqEKmjM+I2EfhA94iG3L7uKrR+GdWD73ydlIB+6hgref1QTlmgmbM3/LeX5GI1Ux1RWpgxpLuZ2+I+IjzZ8wqE4nilvQdkUdfhzI5QDWy+kw5Wgg2pGpeEVeCCA7b85BO3F9DzxB3cdqvBzWcmzbyMiqhzuYqtHRVG2y4x+KOlnyqla8AoWWpuBoYRxzXrfKuILl6SfiWCbjxoZJUaCBj1CjH7GIaDbc9kqBY3W/Rgjda1iqQcOJu2WW+76pZC9QG7M00dffe9hNnseupFL53r8F7YHSwJWUKP2q+k7RdsxyOB11n0xtOvnW4irMMFNV4H0uqwS5ExsmP9AxbDTc9JwgneAT5vTiUSm1E7BSflSt3bfa1tv8Di3R8n3Af7MNWzs49hmauE2wP+ttrq+AsWpFG2awvsuOqbipWHgtuvuaAE+A1Z/7gC9hesnr+7wqCwG8c5yAg3AL1fm8T9AZtp/bbJGwl1pNrE7RuOX7PeMRUERVaPpEs+yqeoSmuOlokqw49pgomjLeh7icHNlG19yjs6XXOMedYm5xH2YxpV2tc0Ro2jJfxC50ApuxGob7lMsxfTbeUv07TyYxpeLucEH1gNd4IKH2LAg5TdVhlCafZvpskfncCfx8pOhJzd76bJWeYFnFciwcYfubRc12Ip/ppIhA1/mSZ/RxjFDrJC5xifFjJpY2Xl5zXdguFqYyTR1zSp1Y9p+tktDYYSNflcxI0iyO4TPBdlRcpeqjK/piF5bklq77VSEaA+z8qmJTFzIWiitbnzR794USKBUaT0NTEsVjZqLaFVqJoPN9ODG70IPbfBHKK+/q/AWR0tJzYHRULOa4MP+W/HfGadZUbfw177G7j/OGbIs8TahLyynl4X4RinF793Oz+BU0saXtUHrVBFT/DnA3ctNPoGbs4hRIjTok8i+algT1lTHi4SxFvONKNrgQFAq2/gFnWMXgwffgYMJpiKYkmW3tTg3ZQ9Jq+f8XN+A5eeUKHWvJWJ2sgJ1Sop+wwhqFVijqWaJhwtD8MNlSBeWNNWTa5Z5kPZw5+LbVT99wqTdx29lMUH4OIG/D86ruKEauBjvH5xy6um/Sfj7ei6UUVk4AIl3MyD4MSSTOFgSwsH/QJWaQ5as7ZcmgBZkzjjU1UrQ74ci1gWBCSGHtuV1H2mhSnO3Wp/3fEV5a+4wz//6qy8JxjZsmxxy5+4w9CDNJY09T072iKG0EnOS0arEYgXqYnXcYHwjTtUNAcMelOd4xpkoqiTYICWFq0JSiPfPDQdnt+4/wuqcXY47QILbgAAAABJRU5ErkJggg==')]"></div>
    );
}

export function ParchmentEffectGenerator() {
    return (
        <svg className='h-0'>
            <filter id='pergamena'>
                <feTurbulence
                    x='0'
                    y='0'
                    baseFrequency='0.04'
                    numOctaves='25'
                    seed='2'
                />
                <feDisplacementMap in='SourceGraphic' scale='15' />
            </filter>
        </svg>
    );
}

export default function Parchment(props: {
    title: string;
    subtitle?: string;
    quote?: string;
    secret?: string;
}) {
    const [showSubTitle, setShowSubtitle] = useState(
        localStorage.getItem(props.title) ? true : false
    );
    const [showQuote, setShowQuote] = useState(
        localStorage.getItem(props.title) ? true : false
    );
    const [secret, setSecret] = useState(
        localStorage.getItem(props.title) ? true : false
    );

    const instantShow = useMemo(() => {
        const data = localStorage.getItem(props.title);
        if (data) {
            return true;
        }
        return false;
    }, [props.title]);

    const handleMouseEnter = () => {
        if (!showSubTitle) setShowSubtitle(true);
    };

    const { width } = useWindowSize();

    const navigate = useNavigate();

    return (
        <div
            style={{
                height:
                    width < 768
                        ? !showQuote && !showSubTitle
                            ? 128
                            : !showQuote
                            ? 200
                            : 450
                        : 650
            }}
            className={`relative select-none h-full md:h-screen snap-start inline-block group  cursor-pointer w-full transition-all duration-1000 ease-in-out min-w-[256px] `}
            onMouseEnter={handleMouseEnter}>
            <ParchmentLayer />
            <div className='relative flex flex-col w-full  h-full md:max-w-lg mb-12 items-center '>
                <h1 className='max-w-fit mx-auto pt-4 md:pt-10 px-4 text-3xl md:text-4xl tracking-wider font-semibold font-FellGreat text-center '>
                    {props.title}
                </h1>
                <div className={`inline-block flex-1 relative   `}>
                    <Transition
                        unmount={false}
                        show={showSubTitle || instantShow}
                        enter={`transition-all  ease-in ${
                            instantShow ? '' : 'duration-1000'
                        }`}
                        enterFrom='opacity-0'
                        enterTo='opacity-100'
                        afterEnter={() => {
                            setShowQuote(true);
                        }}
                        leave='transition-opacity duration-150'
                        leaveFrom='opacity-100'
                        leaveTo='opacity-0'>
                        <h2 className='pt-2 md:pt-10 px-4 text-lg md:text-xl tracking-wide font-semibold font-FellGreat text-center  transition-all duration-1000 ease-in'>
                            {props.subtitle}
                        </h2>
                    </Transition>
                </div>

                <div className='inline-block flex-[2] relative items-center'>
                    <Transition
                        unmount={false}
                        show={showQuote || instantShow}
                        enter={`transition-all  ease-in ${
                            instantShow ? '' : 'duration-1000'
                        }`}
                        enterFrom='opacity-0'
                        enterTo='opacity-100'
                        afterEnter={() => setSecret(true)}
                        leave='transition-opacity duration-150'
                        leaveFrom='opacity-100'
                        leaveTo='opacity-0'>
                        <h2 className='pt-2 md:pt-10 px-4 text-lg md:text-xl tracking-wide font-semibold font-FellGreat text-center italic align-middle h-full flex  transition-all duration-1000 ease-in'>
                            {props.quote}
                        </h2>
                    </Transition>
                </div>

                <div className='inline-block flex-[0.5] relative flex-shrink-0 justify-start'>
                    <Transition
                        unmount={false}
                        show={secret || instantShow}
                        enter={`transition-all  ease-in ${
                            instantShow ? '' : 'duration-1000'
                        }`}
                        enterFrom='opacity-0'
                        enterTo='opacity-100'
                        leave='transition-opacity duration-150'
                        leaveFrom='opacity-100'
                        leaveTo='opacity-0'>
                        <h2 className='pt-2 md:pt-10 px-4 text-xl md:text-2xl tracking-wide font-semibold font-FellGreat text-center  transition-all duration-1000 ease-in'>
                            {props.secret}
                        </h2>
                    </Transition>
                </div>
                <Transition
                    unmount={false}
                    show={secret || instantShow}
                    className={'flex-[0.5]'}
                    enter={`transition-all  ease-in ${
                        instantShow ? '' : 'duration-1000'
                    }`}
                    enterFrom='opacity-0'
                    enterTo='opacity-100'
                    leave='transition-opacity duration-150'
                    leaveFrom='opacity-100'
                    leaveTo='opacity-0'>
                    <GiDiceTwentyFacesTwenty
                        onClick={() => {
                            if (showQuote && secret && showSubTitle) {
                                console.log('navigate to achivements');
                                localStorage.setItem(props.title, 'show');
                                navigate('/hero/' + props.title.toLowerCase());
                            }
                        }}
                        className='block h-9 w-9 my-6 mx-auto bottom-0  animate-bounce'
                    />
                </Transition>
            </div>
        </div>
    );
}
