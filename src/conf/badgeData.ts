import { BadgeColor } from 'components/badge';
import { IconType } from 'react-icons';
import { GiDiceTwentyFacesTwenty } from 'react-icons/gi';

export const achievements: {
    id: number;
    label: string;
    icon: IconType;
    description: string;
    color: BadgeColor;
}[] = [
    {
        id: 1,
        label: 'Brutale',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.red,
        description:
            'sei sempre stato feroce e diretto nei combattimenti, senza mai tirarsi indietro'
    },
    {
        id: 2,
        label: 'Afk',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.purple,
        description: `Non sei andato all'avventura per moooolto tempo, cosa ti è successo?`
    },
    {
        id: 3,
        label: 'Stratega',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.red,
        description:
            'mossa e contromossa, hai pensato ad ogni azione in battaglia'
    },
    {
        id: 4,
        label: 'Leader',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.blue,
        description: 'hai convinto il gruppo a seguirti più di una volta'
    },
    {
        id: 5,
        label: 'Per un soffio',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.red,
        description: 'hai evitato le sale del giudice più di una volta'
    },
    {
        id: 6,
        label: 'Politicamente connesso',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.purple,
        description:
            'ti sei buttato nei giochi di potere di regni e baronie, e ammettilo, ci hai goduto'
    },
    {
        id: 7,
        label: 'Dispellami questo',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.red,
        description: 'hai duellato a colpi di dispell con altri incantatori'
    },
    {
        id: 8,
        label: 'D-D-D-DAMAGE',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.red,
        description:
            'che sia stata una spell, una serie di attacchi o un grande critico, ha fatto molto male.'
    },
    {
        id: 9,
        label: 'Artigiano 2000',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.gold,
        description:
            "Tra rituali, armi, armature, scettri, insomma hai prodotto un po' di tutto"
    },
    {
        id: 10,
        label: 'Si cagheranno sotto',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.blue,
        description:
            'hai intimidito loro, i loro figli e i figli dei loro figli'
    },
    {
        id: 11,
        label: 'Un buon raccolto',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.gold,
        description: 'hai recuperato un sacco di risorse per i tuoi compagni!'
    },
    {
        id: 12,
        label: 'La musa di Silver',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.blueDark,
        description: "in un modo o nell'altro, Silver canterà di te..."
    },
    {
        id: 13,
        label: 'Padre di famiglia',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.blueDark,
        description: "Hai messo su famiglia, in un modo o nell'altro."
    },
    {
        id: 14,
        label: 'Padre per caso',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.blue,
        description:
            'chi lo immaginava che bere ti avrebbe fatto trovare dei figli?'
    },
    {
        id: 15,
        label: 'The Wolf of Wallstreet',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.gold,
        description:
            'il mercato era stato pensato per il crafting, ma tu ne hai visto il "potenziale".'
    },
    {
        id: 16,
        label: 'Una notte da dragoni',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.blue,
        description: 'siete sopravvissuti ad un Gozzovigliare!'
    },
    {
        id: 17,
        label: 'Scese dal cielo',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.blue,
        description:
            "l'abbiamo capito, ti piacciono le entrare in campo scenografiche, ma attento alle ginocchia"
    },
    {
        id: 18,
        label: 'Film mentale',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.purple,
        description:
            'in qualche modo ricorda dettagli stupendi che non sono mai esistiti'
    },
    {
        id: 19,
        label: 'NANANANAN BATMAN!',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.blue,
        description:
            'La criminalità ha paura di muoversi di notte per colpa tua.'
    },
    {
        id: 20,
        label: 'Scienziato Pazzo',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.gold,
        description: 'Hai fatto un sacco di ricerche'
    },
    {
        id: 21,
        label: 'La Guerra dello Stagno',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.blueDark,
        description:
            'Hai partecipato al lungo e terribile conflitto per decidere quale cartello appendere allo stagno di Porto Ameliè'
    },
    {
        id: 22,
        label: "L'uomo detto Inventario",
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.green,
        description:
            'hai immagazzinato di tutto, ed in qualche modo è sempre con te.'
    },
    {
        id: 23,
        label: 'Texano',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.green,
        description: 'hai più armi che anima a momenti'
    },
    {
        id: 24,
        label: 'Ora mi vedi',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.gold,
        description: 'in pratica Invisibilità, ma tu non lanciavi incantesimi'
    },
    {
        id: 25,
        label: 'Radar',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.gold,
        description: 'Un radar vivente, che cazzo di percezione passiva hai?'
    },
    {
        id: 26,
        label: 'Patto col 3 occhi',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.purple,
        description: 'Hai fatto un patto col tre occhi.'
    },
    {
        id: 27,
        label: 'PvP',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.blue,
        description: 'hai combattuto contro altri menbri del gruppo'
    },
    {
        id: 28,
        label: 'A morte Rhogar',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.blue,
        description: 'Hai fatto ciò che nessuno aveva le palle di fare'
    },
    {
        id: 29,
        label: 'One-Man-Army',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.red,
        description: 'Ne hai uccisi tanti...ma davvero tanti...'
    },
    {
        id: 30,
        label: 'Il triangolo no!',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.blue,
        description:
            "sei rimasto invischiato in un triangolo d'amore per la mano di priscilla. Ma cosa è successo?"
    },
    {
        id: 31,
        label: 'Non va giù',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.red,
        description: 'non ti sei limitato a tankare...proprio non volevi morire'
    },
    {
        id: 32,
        label: 'Ma che tragedia',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.blueDark,
        description:
            'il tuo personaggio ha avuto molte sfighe, ammettiamolo...no, non è colpa del master.'
    },
    {
        id: 33,
        label: 'Membro della Sacra Fiamma',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.blue,
        description: "hai servito per il bene, hai affrontato l'oscurità"
    },
    {
        id: 34,
        label: 'Drakkar',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.greenDark,
        description:
            'ci sei riuscito, hai ottenuto la tua nave, ora puoi depredare i mercantili dei tuoi vicini di casa!'
    },
    {
        id: 35,
        label: 'Dritto al Punto',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.blue,
        description: 'hai deciso che i monologhi da cattivo sono una palla'
    },
    {
        id: 36,
        label: 'Fondatore',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.greenDark,
        description:
            'non ti sei limitato a gestire il tuo borgo o la tua baronia, ne hai gettato le basi per la grandezza!'
    },
    {
        id: 37,
        label: 'Amico del 3 Occhi',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.purple,
        description: 'Ma quanti patti hai fatto?'
    },
    {
        id: 38,
        label: 'Tier 3',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.green,
        description: 'hai portato il tuo oggetto personale al massimo livello!'
    },
    {
        id: 39,
        label: "Hero's Journey",
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.blueDark,
        description:
            "Il tuo personaggio è letteralmente irriconoscibile rispetto all'inizio della storia."
    },
    {
        id: 40,
        label: 'Romance',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.pink,
        description:
            'Hai cercato avventure, e la più grande in qualche modo ti ha trovato'
    },
    {
        id: 41,
        label: 'Maestro della Lore',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.purple,
        description:
            'Hai seguito tutti gli indizzi, ha tenuto traccia di ogni scoperta sul mondo fantastico di Jadalne. Chissà quante cose nel tuo diario'
    },
    {
        id: 42,
        label: 'Mi licenzio',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.blueDark,
        description:
            'Possiamo dire senza alcun dubbio che cambiare patrono 3 volte in una singola campagna sia un record per un warlock'
    },
    {
        id: 43,
        label: 'Beta Test',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.purple,
        description:
            'Hai deciso di provare il materiale del master, anche se era rotto come la 3.5 '
    },
    {
        id: 44,
        label: `Apocalypse Now`,
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.red,
        description: `adori l'odore delle palle di fuoco sui nemici la mattina`
    },
    {
        id: 45,
        label: `To Hell and Back`,
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.greenDark,
        description: `Siete sopravvissuti ai 40 Giorni di Apocalisse, siete riusciti a tornare a casa!`
    },
    {
        id: 46,
        label: `Tanto non la uso`,
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.blueDark,
        description: `La propria anima, si sà, è il migliore dei reagenti`
    },
    {
        id: 47,
        label: 'Nessuno tornò',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.greenDark,
        description:
            'Chiunque può fare un bagno di sangue, ma farlo senza essere visti...'
    },
    {
        id: 48,
        label: 'Mix Perfetto',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.purple,
        description:
            'Hai provato a multiclassare, perchè la vita è troppo noiosa per una sola classe'
    },
    {
        id: 49,
        label: 'Maniaco della Forgia',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.gold,
        description:
            'Craftare non era solo una meccanica per te, ma un bisogno fisiologico'
    },
    {
        id: 50,
        label: 'Nobiltà "acquisita"',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.greenDark,
        description: 'Ti sei guadagnato col ferro il tuo diritto a regnare'
    },
    {
        id: 51,
        label: 'Violazione di Privacy',
        icon: GiDiceTwentyFacesTwenty,
        color: BadgeColor.gold,
        description:
            'Quanti cristalli di Mortarion avrai guardato? Chissà quanto gossip.'
    }
];

export const daegon: number[] = [
    36, 24, 4, 9, 39, 5, 47, 16, 10, 26, 45, 3, 33, 8, 13, 29, 17, 11, 38, 40
];
export const elwan: number[] = [
    20, 43, 7, 45, 26, 48, 28, 24, 27, 3, 16, 46, 25, 8, 5, 9, 4, 38
];
export const fredrick: number[] = [
    12, 40, 50, 6, 34, 1, 43, 16, 38, 21, 30, 36, 26, 2, 13, 39
];
export const lurtz: number[] = [
    39, 40, 32, 12, 14, 49, 23, 15, 5, 48, 45, 8, 27, 26, 9, 35, 33, 2, 1, 13,
    18, 16, 17, 31, 38
];
export const tyron: number[] = [
    42, 16, 8, 40, 21, 4, 22, 7, 36, 45, 15, 43, 51, 3, 12, 30, 11, 41, 14
];

const raccolta = { daegon, elwan, fredrick, lurtz, tyron };

export default raccolta;
