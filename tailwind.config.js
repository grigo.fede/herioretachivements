module.exports = {
    content: ['./*.html', './src/**/*.{js,jsx,ts,tsx}'],
    mode: 'jit',
    theme: {
        extend: {
            fontFamily: {
                FellGreat: ['"IM Fell Great Primer SC"', 'serif'],
                metalmania: ['"Metal Mania"', 'cursive']
            },
            animation: {
                'reverse-spin': 'reverse-spin 1s linear infinite'
            },
            keyframes: {
                'reverse-spin': {
                    from: {
                        transform: 'rotate(360deg)'
                    }
                }
            }
        }
    },
    plugins: [require('@tailwindcss/forms')]
};
